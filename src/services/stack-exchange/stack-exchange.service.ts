import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  StackExchangeOrder,
  StackExchangeSite,
  StackExchangeSort,
} from './types/request';
import {
  StackExchangeQuestion,
  StackExchangeQuestionResponse,
} from './types/response';

@Injectable({
  providedIn: 'root',
})
export class StackExchangeService {
  private version = '2.2';
  constructor(
    @Inject('STACK_EXCHANGE_API_URL') private api: string,
    private http: HttpClient
  ) {}

  /**
   * getQuestions
   *
   * order=desc&sort=hot&tagged=reactjs&site=stackoverflow
   */
  public getQuestions(
    tagged: string[],
    site: StackExchangeSite,
    order?: StackExchangeOrder,
    sort?: StackExchangeSort
  ): Observable<StackExchangeQuestion[]> {
    return this.http
      .get<StackExchangeQuestionResponse>(
        `${this.api}${this.version}/questions`,
        {
          params: {
            tagged: tagged.join(';'),
            ...(site && { site }),
            ...(sort && { sort }),
            ...(order && { order }),
          },
        }
      )
      .pipe(map((res) => res.items as StackExchangeQuestion[]));
  }
}
