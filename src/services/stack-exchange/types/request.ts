export enum StackExchangeOrder {
  DESC = 'desc',
  ASC = 'asc',
}

export enum StackExchangeSort {
  HOT = 'hot',
  ACTIVITY = 'activity',
  CREATION = 'creation',
  VOTES = 'votes',
}

export enum StackExchangeSite {
  STACKOVERFLOW = 'stackoverflow',
}
