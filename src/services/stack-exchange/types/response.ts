export type Url = string;

export type Timestamp = number;

export type UserType = 'registered';

export interface StackExchangeQuestionResponse {
  has_more: boolean;
  items: StackExchangeQuestion[];
  quota_max: number;
  quota_remaining: number;
}

export interface StackExchangeQuestion {
  accepted_answer_id: number;
  answer_count: number;
  content_license: string;
  creation_date: Timestamp;
  is_answered: boolean;
  last_activity_date: Timestamp;
  link: Url;
  owner: {
    reputation: number;
    user_id: number;
    user_type: UserType;
    profile_image: Url;
    display_name: string;
    link: Url;
  };
  question_id: number;
  score: number;
  tags: string[];
  title: string;
  view_count: number;
}
