import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { StackExchangeService } from 'src/services/stack-exchange/stack-exchange.service';
import {
  StackExchangeOrder,
  StackExchangeSite,
  StackExchangeSort,
} from 'src/services/stack-exchange/types/request';
import { StackExchangeQuestion } from 'src/services/stack-exchange/types/response';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'rekry';
  questions$: Observable<StackExchangeQuestion[]> = of([]);

  constructor(private questionsService: StackExchangeService) {}

  ngOnInit() {
    this.questions$ = this.questionsService
      .getQuestions(
        ['angular'],
        StackExchangeSite.STACKOVERFLOW,
        StackExchangeOrder.DESC,
        StackExchangeSort.CREATION
      )
      .pipe(tap((data: any) => console.log(data)));
  }
}
