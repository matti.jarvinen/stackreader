import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { UiUtilsModule } from 'src/shared/ui-utils/ui-utils.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, UiUtilsModule],
  providers: [
    {
      provide: 'STACK_EXCHANGE_API_URL',
      useValue: environment.stackExchangeAPI,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
