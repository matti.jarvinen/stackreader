import { inject, TestBed } from '@angular/core/testing';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { SafeHTMLPipe } from './safe-html.pipe';

describe('SafeHTMLPipe', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule],
    });
  });

  it('create an instance', inject(
    [DomSanitizer],
    (domSanitizer: DomSanitizer) => {
      let pipe = new SafeHTMLPipe(domSanitizer);
      expect(pipe).toBeTruthy();
    }
  ));

  it('show html as entities', inject(
    [DomSanitizer],
    (domSanitizer: DomSanitizer) => {
      let pipe = new SafeHTMLPipe(domSanitizer);

      expect(pipe.transform('&amp;')).toEqual(
        domSanitizer.bypassSecurityTrustHtml('&amp')
      );
    }
  ));
});
